export default {
    updateTheme(className) {
        document.querySelector("body").className = className;
    }
}