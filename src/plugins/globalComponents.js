import Breadcrumbs from "@/components/Breadcrumbs";

export default {
  install(Vue) {
    Vue.component(Breadcrumbs.name, Breadcrumbs);
  }
};
